/**
 * Created by AndresFabian on 27/06/2016.
 */
var locationmodule = {

    initlocation: function(successCallback, errorCallback, userId){
        cordova.exec(successCallback,
            errorCallback,
            "LocationModulePlugin",
            "initlocation",
            [{
                "userId":userId,
            }]
        );

    },
    initbeacon: function(successCallback, errorCallback, userId){
        cordova.exec(successCallback,
            errorCallback,
            "LocationModulePlugin",
            "initbeacon",
            [{
                "userId":userId,
            }]
        );

    }
}

module.exports = locationmodule;