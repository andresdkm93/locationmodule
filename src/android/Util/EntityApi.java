package com.info.andres.locationmodule.Util;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.info.andres.locationmodule.Constants;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by AndresFabian on 19/06/2016.
 */
public class EntityApi implements ResourceApi {


    private ResourceResponse Parent;
    private String Name;
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    public EntityApi(String Name)
    {
        this.Parent =null;
        this.Name=Name;
    }

    public void save(ResourceResponse parent,Object data) {
        this.Parent = parent;
        AsyncTask<Object, Response, Response> t = new AsyncTask<Object, Response, Response>() {
            protected Response doInBackground(Object... params) {
                String response = "";
                try {
                    Gson gson = new Gson();
                    OkHttpClient httpClient = new OkHttpClient();
                    RequestBody body = RequestBody.create(JSON, gson.toJson(params[0]));
                    Request request = new Request.Builder()
                            .url(Constants.BASEURL+getName())
                            .post(body)
                            .build();
                    return httpClient.newCall(request).execute();
                } catch (Exception e) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result) {
                ResourceResponse parent = getParent();
                if (parent != null)
                    parent.response(result);
            }

        }.execute(data);
    }

    public ResourceResponse getParent() {
        return Parent;
    }

    public void setParent(ResourceResponse parent) {
        this.Parent = parent;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }


}
