package com.info.andres.locationmodule.Util;

import java.io.IOException;

import okhttp3.Response;

/**
 * Created by AndresFabian on 19/06/2016.
 */
public interface ResourceResponse {

    public void response(Response response);
}
