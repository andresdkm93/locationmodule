package com.info.andres.locationmodule.Util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by AndresFabian on 27/06/2016.
 */
public class Preferences {


    public static void save(String key,String value,Context  t)
    {
        SharedPreferences prefs = t.getSharedPreferences("PreferencesArenas", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String get(String key,Context t)
    {
        SharedPreferences prefs = t.getSharedPreferences("PreferencesArenas", Context.MODE_PRIVATE);
        String value = prefs.getString(key, "0");
        return value;
    }
}
