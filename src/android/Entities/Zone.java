package com.info.andres.locationmodules.Entities;

import com.info.andres.locationmodule.Entities.Beacon;

import java.util.List;

/**
 * Created by AndresFabian on 19/06/2016.
 */
public class Zone {
    private String name;
    private double latitude;
    private double longitude;
    private int radius;
    private int id;
    private List<Beacon> beacons;

    public String getName() {
        return name;
    }

    public List<Beacon> getBeacons() {
        return beacons;
    }

    public void setBeacons(List<Beacon> beacons) {
        this.beacons = beacons;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
