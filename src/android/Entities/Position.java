package com.info.andres.locationmodule.Entities;

import com.info.andres.locationmodule.Util.EntityApi;
import com.info.andres.locationmodule.Util.ResourceResponse;

/**
 * Created by andres on 13/06/16.
 */
public class Position {

    private double latitude;
    private double longitude;
    private int user_id;
    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
