package com.info.andres.locationmodule;

/**
 * Created by AndresFabian on 27/06/2016.
 */
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.info.andres.locationmodule.Beacons.BeaconService;
import com.info.andres.locationmodule.Location.LocationService;
import com.info.andres.locationmodule.Util.Preferences;
import com.tecnia.arenasapp.MainActivity;

import org.apache.cordova.CordovaInterface;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LocationModulePlugin extends CordovaPlugin  {
    public static final String INIT_LOCATION = "initlocation";
    public static final String INIT_BEACON = "initbeacon";
    private static final int    REQUEST_ENABLE_BT   = 1;
    private BluetoothAdapter bAdapter;

    public boolean execute(String action, JSONArray jsonArgs,
                           CallbackContext callbackContext) throws JSONException {
        try {
            if (INIT_LOCATION.equals(action)) {
                Context context = cordova.getActivity()
                        .getApplicationContext();
                JSONObject args = jsonArgs.getJSONObject(0);
                String userId = args.getString("userId");
                Preferences.save("userId", userId, context);
                Intent intent = new Intent(cordova.getActivity(), LocationService.class);
                cordova.getActivity().startService(intent);
                callbackContext.success();
            }else if(INIT_BEACON.equals(action))
            {
                Context context = cordova.getActivity()
                        .getApplicationContext();
                JSONObject args = jsonArgs.getJSONObject(0);
                String userId = args.getString("userId");
                Preferences.save("userId", userId, context);
                bAdapter=BluetoothAdapter.getDefaultAdapter();
                if(bAdapter.isEnabled())
                {
                    startServiceBeacon();
                }
                else
                {
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    cordova.setActivityResultCallback (this);

                    cordova.getActivity().startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                }
                callbackContext.success();
            }
            return true;
        } catch (Exception e) {
            callbackContext.error(e.getMessage());
            return false;
        }
    }

    public void startServiceBeacon()
    {
        Intent intent = new Intent(cordova.getActivity(), BeaconService.class);
        cordova.getActivity().startService(intent);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode)
        {
            case REQUEST_ENABLE_BT:
            {
                if(resultCode == cordova.getActivity().RESULT_OK)
                {
                    Log.d(Constants.TAG, "Bluetooth Activado");
                    startServiceBeacon();
                }
                else
                {
                    Log.d(Constants.TAG, "Bluetooth Desactivado");
                }
                break;
            }

            default:
                break;
        }
    }
}
