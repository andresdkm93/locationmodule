package com.info.andres.locationmodule.Beacons;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;


import java.util.List;
import java.util.UUID;

public class BeaconService extends Service implements BeaconManager.ServiceReadyCallback {
    private BeaconManager beaconManager;
    private Region region;
    private static final int NOTIF_ALERTA_ID = 1;

    private int zone_id;

    public BeaconService(int zone_id) {
        this.zone_id = zone_id;
    }

    public BeaconService() {
        this.zone_id = 0;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        beaconManager = new BeaconManager(this);
        beaconManager.setRangingListener(new BeaconManagerListener(this));
        region = new Region("ranged region", UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"), null, null);
        beaconManager.setBackgroundScanPeriod(20000,10000);
        beaconManager.setForegroundScanPeriod(20000,10000);
        beaconManager.connect(this);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        beaconManager.stopRanging(region);
    }



    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onServiceReady() {

        beaconManager.startRanging(region);
    }


}
