package com.info.andres.locationmodule.Beacons;

import android.util.Log;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.info.andres.locationmodule.Constants;
import com.info.andres.locationmodule.Entities.ViewedBeacon;
import com.info.andres.locationmodule.Util.EntityApi;
import com.info.andres.locationmodule.Util.Preferences;

import java.util.List;

/**
 * Created by AndresFabian on 19/06/2016.
 */
public class BeaconManagerListener implements BeaconManager.RangingListener {
    private BeaconService service;
    public BeaconManagerListener(BeaconService service) {
        this.service = service;
    }

    @Override
    public void onBeaconsDiscovered(Region region, List<Beacon> list) {
        if (!list.isEmpty()) {
            for (Beacon beacon:list) {
                ViewedBeacon viewed= new ViewedBeacon();
                viewed.setUuid(beacon.getProximityUUID().toString());
                viewed.setMajor("" + beacon.getMajor());
                viewed.setMinor("" + beacon.getMinor());
                viewed.setRssi(""+beacon.getRssi());
                viewed.setPower(""+beacon.getMeasuredPower());
                int userId=Integer.parseInt(Preferences.get("userId", service.getApplicationContext()));
                viewed.setUser_id(userId);
                EntityApi api= new EntityApi("viewedBeacons");
                api.save(null,viewed);
                Log.d(Constants.TAG, beacon.getProximityUUID()+" "+beacon.getMajor()+" "+beacon.getMinor());
            }
        }
    }
}
