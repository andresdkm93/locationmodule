package com.info.andres.locationmodule.Location;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.location.LocationListener;
import com.info.andres.locationmodule.Constants;
import com.info.andres.locationmodule.Entities.Position;
import com.info.andres.locationmodule.Util.EntityApi;
import com.info.andres.locationmodule.Util.Preferences;
import com.info.andres.locationmodule.Util.ResourceResponse;

import okhttp3.Response;


/**
 * Created by andres on 13/06/16.
 */
public class ListenerLocation implements LocationListener {

    private LocationService service;

    public ListenerLocation(LocationService service) {
        this.service = service;
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(Constants.TAG, "onLocationChanged: ");
        Position p = new Position();
        int userId=Integer.parseInt(Preferences.get("userId",service.getApplicationContext()));
        if(userId>0)
        {
            p.setUser_id(userId);
            p.setLatitude(location.getLatitude());
            p.setLongitude(location.getLongitude());
            EntityApi api = new EntityApi("positions");
            api.save(new ResourceResponse() {
                @Override
                public void response(Response response) {
                    Log.i(Constants.TAG,response.toString());

                }
            }, p);
        }

    }


}
